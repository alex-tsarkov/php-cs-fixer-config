<?php

namespace AlexTsarkov\PhpCsFixer;

use PhpCsFixer\Config as PhpCsFixerConfig;

class Config extends PhpCsFixerConfig
{
    /**
     * {@inheritdoc}
     */
    public static function create()
    {
        return parent::create()
            ->setRules([
                '@PSR2' => true,
                '@Symfony' => true,
                '@PhpCsFixer' => true,
                '@PHP71Migration' => true,
                'array_syntax' => [
                    'syntax' => 'short',
                ],
                'class_keyword_remove' => false,
                'concat_space' => [
                    'spacing' => 'one',
                ],
                'list_syntax' => [
                    'syntax' => 'short',
                ],
                'method_chaining_indentation' => false,
                'no_superfluous_phpdoc_tags' => true,
                'simplified_null_return' => true,
            ])
        ;
    }
}

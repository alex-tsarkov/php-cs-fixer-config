<?php

return AlexTsarkov\PhpCsFixer\Config::create()
    ->setUsingCache(false)
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in('src')
    )
;
